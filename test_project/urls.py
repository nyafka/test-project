from django.contrib import admin
from django.urls import include, path

from rest_framework import routers

from points.views import UserPointsViewSet
from profiles.views import (
    UserViewSet, LoginView
)


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'points', UserPointsViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path(
        'api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    ),
    path("login/", LoginView.as_view(), name="login"),
]
