from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.templatetags.static import static

from rest_framework.authtoken.models import Token


class User(AbstractUser):
    """ Extended django User model """

    avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)
    interests = models.TextField(default='', blank=True)

    def get_url_avatar(self):
        if self.avatar and hasattr(self.avatar, 'url'):
            return self.avatar.url
        return static('img/none-avatar.png')

    def get_sum_points(self):
        """ get sum of all points """

        return self.points.aggregate(
            total_points=models.Sum('amount')
        )['total_points'] or 0


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)