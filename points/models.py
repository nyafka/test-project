from django.db import models

from profiles.models import User


class Points(models.Model):
    """ Models of users points """

    TYPE_POINTS = (
        (1, 'Games'),
        (2, 'Bonus'),
        (3, 'Prize'),
    )

    user = models.ForeignKey(
        User, related_name='points', on_delete=models.CASCADE
    )
    type_point = models.IntegerField(choices=TYPE_POINTS)
    amount = models.IntegerField()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username