from rest_framework import serializers

from points.models import Points


class UserPointsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Points
        fields = ['user', 'amount', 'type_point']