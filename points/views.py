from rest_framework import viewsets

from points.models import Points
from points.serializers import UserPointsSerializer


class UserPointsViewSet(viewsets.ModelViewSet):
    queryset = Points.objects.all()
    serializer_class = UserPointsSerializer
