from rest_framework import status
from django.test import TestCase

from rest_framework.test import APIClient

from points.models import Points
from profiles.models import User


client = APIClient()

class CreateNewPoints(TestCase):
    def setUp(self):
        user = User.objects.create(
            username='Casper', password='123', interests='New interest')
        self.valid_payload = {
            'user': f'/users/{user.id}/',
            'amount': 20,
            'type_point': Points.TYPE_POINTS[0][0]
        }
        self.invalid_payload = {
            'user': f'/users/{user.id}/',
            'amount': 20,
        }

    def test_create_valid_points(self):
        response = client.post(
            '/points/',
            self.valid_payload,
            format='json'
        )
        print(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_points(self):
        response = client.post(
            '/points/',
            self.invalid_payload,
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)